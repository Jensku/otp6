# OTP6

Ohjelmistotuotantoprojekti osa 2

SISÄLTÖ
---------------------

 * Kuvaus
 * Kehitysympäristö
 * Asennus ja käyttöohje
 

KUVAUS
------
Tämä on Metropolia Ammattikorkeakoulun opiskelijoiden luoma varastonhallintatyökalu Ohjelmistotuotantoprojekti 2-kurssia varten. Projektin tavoitteena on opetella ketteriä ohjelmistokehitysmenetelmiä, sekä ohjelman lokalisointia ja testaamista. Tämän ohjelman avulla yritys voi tehokkaasti seurata kalustonsa kuntoa ja tarvetta uusille hankinnoille. Ohjelma myös helpottaa yrityksen työntekijöiden työtä sujuvoittamalla työvälineiden lainausta ja palautusta.

KEHITYSYMPÄRISTÖ
----------------
Projekti on Javalla toteutettava varastonhallintaohjelma, joka kommunikoi MariaDB-tietokannan kanssa. Tietokantaa pidetään Metropolian Educloud-serverillä.
Koonnista vastaavat Maven ja Jenkins.
Käyttöliittymä on toteutettu JavaFX:llä. Hibernate hoitaa tietokannan ja ohjelmiston yhteensovituksen.  
Versionhallintaan käytetään GIT:iä ja etävarastona on GITLab. Ryhmätyöskentelyn suoraviivaistamiseksi on käytetty Discordia, ja projektinhallintaan Nektionia.

ASENNUS JA KÄYTTÖOHJE
---------------------
OHJELMOINTIYMPÄRISTÖ: Eclipse, NetBeans tai vastaava Java-kieltä tukeva ohjelmointiympäristö.
GIT: Kun käyttöoikeudet repositorioon on saatu, lähdekoodin voi ladata Master-haarasta. Käyttäjä luo oman haaran omia muokkauksia varten, Masteriin vain valmis ja testattu koodi.
TIETOKANTA: Tietokanta pyörii Educloud-serverillä, johon pääsee käsiksi vain Metropolian sisäisen verkon kautta tai vaihtoehtoisesti voi käyttää Cisco AnyConnect VPN-yhteyttä, joka vaatii Metropolia-tunnuksen ja -salasanan.
MariaDB/HeidiSQL:
Hostname / IP: 10.114.32.31
User: oskari
Password: salasana
KOONTI: Jenkinsin osoite on http://10.114.32.31:8080/ tunnus: -omaNimi- salasana: varasto0 