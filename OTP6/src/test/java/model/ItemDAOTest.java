package model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Testiluokka tietokantahakujen testaamiseen
 * @author Reija Parvio, Jensina Hakkarainen
 *
 */

class ItemDAOTest {
	
	private static int id;
	private static Item testItem, testlist1;
	private static List<Item> testlist;
	private IInventoryDAO inventorydao = new ItemDAO();
	
	
	/**
	 * Ennen testaamista, luodaan oikeat tiedot sisältävät materiaalit,
	 * joihin testeissä saatavaa dataa verrataan
	 * @throws Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		testItem = new Item();
		testItem.setName("Kuokka");
		testItem.setId(3);
		id = 3;
		
		testlist = new ArrayList<>();
		testlist1 = new Item();
		testlist1.setName("Hanskat");
		testlist.add(testlist1);
	}
	
	/**
	 * Read-metodin testi, joka hakee tietokannasta id-parametrillä sitä vastaavan Item-tyyppisen tietueen.
	 * Tuloksia verrataan testidataan, johon on kirjattu Item-olion oikea id ja nimi.
	 */

	@Test
	void testRead() {
		Item item = inventorydao.read(id);
		assertEquals(item.getId(), id, "Ei löydy id");
		assertEquals(item.getName(), testItem.getName(), "Ei löydy nimeä");
	}
	
	/**
	 * ListItems-metodin testi, joka hakee tietokannasta koko Inventory-taulun sisällön Item-tyyppiseen listaan.
	 * Testituloksesta otetaan ensimmäisen tietueen palauttama Item-olion nimi, jota verrataan testidataan,
	 * johon on tallennettu oikea nimi. 
	 */
	
	@Test
	void testListItems() {
		List<Item> itemList = inventorydao.listItems();
		Item item1 = itemList.get(0);
		String name = item1.getName();
		assertEquals(name, testlist1.getName(), "Nimi ei täsmää");
	}

}
