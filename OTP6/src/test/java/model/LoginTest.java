package model;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Emil Rantanen
 *
 * Testataan kirjautumista tunnuksilla, jotka toimivat ja jotka ei toimi.
 *
 */

public class LoginTest {
	
	private Login loginTester = new Login();

	
	@Test
    public void testLogin() {
        boolean answer = loginTester.loginAttempt("12345", "1111");
        assertEquals(true, answer);
    }
    
    @Test
    public void testLoginGibberish() {
    	boolean answer = loginTester.loginAttempt("asdfasdfasdf", "liibalbaa");
        assertEquals(false, answer);
    }
}
