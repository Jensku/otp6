package controller;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import model.Item;

class InventoryControllerTest {
	
	private static Item testItem, testlist1;
	private static List<Item> testlist;
	private InventoryController ic = new InventoryController();
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		testlist = new ArrayList<>();
		testlist1 = new Item();
		testlist1.setName("Hanskat");
		testlist.add(testlist1);
	}

	@Test
	void testGetItemList() {
		List<Item> itemList = ic.getItemList();
		Item item1 = itemList.get(0);
		String name = item1.getName();
		assertEquals(name, testlist1.getName(), "Nimi ei täsmää");
	}

}
