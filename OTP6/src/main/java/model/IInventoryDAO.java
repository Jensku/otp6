package model;

import java.util.ArrayList;
import java.util.List;



/**
 * Rajapinta, jossa metodit listan tietokantatoiminnoille.
 * 
 * @author Jensina Hakkarainen
 *
 */
public interface IInventoryDAO {

	public List<Item> listItems();
	public Item read(int itemNo);

}
