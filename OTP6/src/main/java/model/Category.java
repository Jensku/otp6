package model;

import javax.persistence.*;

/**
 * Catgory-luokka luo tietokantaan category-taulun, jossa on id ja nimi.
 * Luokka sisältää konstruktorin, seka setterit ja getterit taulun sarakkeille.
 * @author Jensina Hakkarainen
 * @version 26.11.2020
 *
 */

@Entity
@Table(name="category")
public class Category {
	
	@Id
	@GeneratedValue
	@Column(name="categoryId")
	private int id;
	
	@Column(name="categoryName")
	private String name;
	
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Category() {
		
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}