package model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Emil Rantanen
 * Tietokannan kanssa työskentelevä kirjautumisen toteutus.
 *
 */

@SuppressWarnings("deprecation")
public class Login {
	private SessionFactory sessionfactory;
	private Connection connection;
	
	/**
	 * Login() ja finalize() avaavat ja sulkevat yhteyden tietokantaan.
	 * 
	 */
	
	public Login() {
		try {
			sessionfactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
			System.out.println("istuntotehdas ok");
		} catch (Exception e) {
			System.err.println("Istuntotehtaan luonti ei onnistunut: " + e.getMessage());
			System.exit(-1);
		}
	}
	
	public void finalize() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception e) {
			System.err.println("Istuntotehtaan sulkeminen epäonnistui: " + e.getMessage());
		}
	}
	
	/**
	 * loginAttempt tarkistaa käyttäjän syöttämät kirjautumistiedot tietokannasta.
	 * @return onnistuiko kirjautuminen vaiko ei.
	 */
	
	@SuppressWarnings("rawtypes")
	public boolean loginAttempt(String login, String pin) {
		boolean info = false;
		try(Session session = sessionfactory.openSession()) {
			String IdQuery = "SELECT userLogin FROM user WHERE userLogin = :login";
			String PinQuery = "SELECT userLogin FROM user WHERE userPin = :pin";
			
			Query spSQLQuery = session.createSQLQuery(IdQuery);
			spSQLQuery.setParameter("login",login);
			List<String> listIds = spSQLQuery.list();
			
			spSQLQuery = session.createSQLQuery(PinQuery);
			spSQLQuery.setParameter("pin",pin);
			List<String> listPins = spSQLQuery.list();
			
			if ((listIds != null && !listIds.isEmpty()) && (listPins != null && !listPins.isEmpty())) {
				System.out.println("Kirjautuminen onnistuu.");
				info = true;
			}
			
			listIds.forEach(id -> System.out.println(id));
			listPins.forEach(code -> System.out.println(code));
					
		} catch (Exception e) {
			System.err.println("Sisäänkirjautumisessa virhe: " + e.getMessage());
		}
		
		return info;
	}


}
