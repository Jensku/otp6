package model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.jpa.QueryHints;
import org.hibernate.Criteria;
import org.hibernate.Query;
import com.mysql.fabric.xmlrpc.base.Data;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * DataAccessObject-luokka, jonka kautta suoritetaan tietokantaoperaatiot.
 * Luokassa on metodit yksittäisen tavaran ja tavaralistan lukemiselle.
 * 
 * @author Jensina Hakkarainen
 * @author Reija Parvio
 *
 */
public class ItemDAO implements IInventoryDAO {
	private SessionFactory sessionfactory;
	private Connection connection;
	
	/**
	 * Konstruktori, jossa luodaan istuntotehdas
	 */
	public ItemDAO() {
		try {
			sessionfactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		} catch (Exception e) {
			System.err.println("Istuntotehtaan luonti ei onnistunut: " + e.getMessage());
			System.exit(-1);
		}
	}
	
	/**
	 * Suljetaan istuntotehdas
	 */
	
	public void finalize() {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception e) {
			System.err.println("Istuntotehtaan sulkeminen epäonnistui: " + e.getMessage());
		}
	}
	
	/**
	 * Luetaan yksittäinen tavara tietokannasta, parametrina tavaran numero
	 * @param itemNo
	 */
	@Override
	public Item read(int itemNo) {
		Transaction transaction = null;
				Item item = new Item();
				try (Session session = sessionfactory.openSession()) {
					transaction = session.beginTransaction();
					item = (Item)session.get(Item.class, itemNo);
					if(item != null) {
						return item;
					}
					transaction.commit();
				} catch (Exception e) {
					if (transaction != null) transaction.rollback();
				}
				return null;
	}
	
	/**
	 * Luetaan kaikki tietokannan alkiot Item-tyyppiseen listaan
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Item> listItems(){
		List<Item> list = new ArrayList<>();
		@SuppressWarnings("unused")
		Transaction transaction = null;
		
		try(Session session = sessionfactory.openSession()) {
			transaction = session.beginTransaction();
			Criteria criteria = (Criteria) session.createCriteria(Item.class);
			criteria.addOrder(Order.asc("name"));
			list = criteria.list();
			System.out.println(list);
			
			session.getTransaction().commit();
			session.close();
		} catch (Exception e) {
			System.out.println("Tietokantahaku ei onnistunut");
			System.out.println(e);
		}
			return list;
	}
}