package model;

import javax.persistence.*;
import javax.transaction.*;


/**
 * Item-luokka jonka pohjalta luodaan Hibernaten ja JPA:n avulla tietokantaan saman niminen
 * taulu, ja sen sarakkeet.
 * Luokka sisältää konstruktorin, sekä getterit ja setterit sarakkeille.
 * Taulun varsinainen käsittely tapahtuu ItemDAO-luokan avulla.
 * 
 * @author Jensina Hakkarainen
 */

@Entity
@Transactional
@Table(name="item")
public class Item {
	@Id
	@GeneratedValue
	@Column(name="itemId")
	private int id;
	
	@Column(name="itemName")
	private String name;
	
	//Jätettiin pois consumable-kohta, kuntoon tulee 0=hyvä, 1=huono, 2=käyttötavara
	@Column(name="itemCondition")
	private int condition;
	
	@ManyToOne
	@JoinColumn(name="itemCategory")
	private Category category;
	
	
	public Item(int id, String name, Category category, int condition) {
		this.id = id;
		this.name = name;
		this.condition = condition;
		this.category = category;
	}
	
	public Item() {
		
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public String getCategory() {
		return category.getName();
	}
	
	public void setCondition(int condition) {
		this.condition = condition;
	}
	
	public int getCondition() {
		return condition;
	}
}