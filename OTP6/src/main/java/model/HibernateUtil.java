package model;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * HibernateUtil-luokka luo yhteyden tietokantaan ja sitä kutsutaan muista luokista
 * @author Jensina Hakkarainen
 *
 */
public class HibernateUtil {
	private static SessionFactory sessionFactory = null;
	
	/**
	 * Luodaan sessionFactory, joka luo yhteyden tietokantaan
	 */
	static{
		try {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		} catch (Exception e) {
			System.err.println("Sessionfactoryn luominen hibernateutil-luokassa ei onnistu");
			System.err.println(e);
			System.exit(-1);
		}
	 }
	/**
	 * Getteri sessionFactorylle
	 * @return sessionFactory
	 */
	
	 public static SessionFactory getSessionFactory(){
	    return sessionFactory;
	 }
}
