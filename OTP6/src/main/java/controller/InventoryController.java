package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Item;
import model.ItemDAO;
import ui.BasicGUI;
import java.util.List;

/**
 * Luokka kommunikoi tietokannan ja käyttöliittymän välillä.
 * @author Reija Parvio, Jensina Hakkarainen
 *
 */

public class InventoryController{
	
	private BasicGUI ibgui;
	private ItemDAO itemdao = new ItemDAO();
	private ObservableList<Item> itemList=FXCollections.observableArrayList();
	
	/**
	 * Konstruktorissa saadaan viittaus käyttöliittymään 
	 * Viittaus tallennetaan luokkamuuttujaan
	 * @param ibgui
	 */

	public InventoryController(BasicGUI ibgui) {
		this.ibgui = ibgui;
	}
	
	public InventoryController() {}
	
	/**
	 * Tietokannasta saatu List muutetaan muotoon ObservableList
	 * @return palautetaan Item-olioita sisällään pitävä ObservableList
	 */
	
	public ObservableList<Item> getItemList(){
        if(!itemList.isEmpty())
             itemList.clear();                    
        itemList = FXCollections.observableList((List<Item>) itemdao.listItems());
        System.out.println(itemdao.listItems().getClass());
        
        return itemList;
   }
}
