package controller;


import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Login;

/**
 * 
 * @author Emil Rantanen
 *
 * Tarkistetaan käyttäjän syöttämät arvot kirjautumista varten.
 *
 */

public class LoginController {

	private String idAttempt = null;
    private String pinAttempt = null;
    
    private Login inLogger = new Login();

    private Stage dialogStage;
    private boolean okClicked = false;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public void setId(String id) {
    	this.idAttempt = id;
    }
    
    public void setPin(String pin) {
    	this.pinAttempt = pin;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    /**
	 * handleOk() kutsuu loginAttempt()-metodia tarkastamaan, onnistuuko sisäänkirjautuminen.
	 */
    
    public void handleOk() {
        if (isInputValid()) {
        	
        	System.out.println("tähän asti ok");
        	if (inLogger.loginAttempt(idAttempt, pinAttempt) == true) {
        		System.out.println("läpi meni");
        		okClicked = true;
        	} else {
        		System.out.println("tunnus tai salasana väärin!");
        	}
        	inLogger.finalize();
        }
    }

    /**
	 * isInputValid tarkistaa käyttäjän syötteen.
	 * @return syötteen oikeellisuus
	 */
    private boolean isInputValid() {
        String errorMessage = "";

        if (idAttempt == null || idAttempt.length() == 0) {
            errorMessage += "Käyttäjätunnuskenttä ei voi olla tyhjä!\n"; 
        } else {
            try {
                Integer.parseInt(idAttempt);
            } catch (NumberFormatException e) {
                errorMessage += "Työntekijän numero -kenttään vain numeroita.\n"; 
            }
        }
        if (pinAttempt== null || pinAttempt.length() == 0) {
            errorMessage += "Käyttäjätunnuskenttä ei voi olla tyhjä!\n"; 
        } else {
            try {
                Integer.parseInt(pinAttempt);
            } catch (NumberFormatException e) {
                errorMessage += "Työntekijän pin -kenttään vain numeroita.\n"; 
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Virheelliset syötteet");
            //alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);
            
            alert.showAndWait();
            
            return false;
        }
    }
	
}
