package ui;


import java.io.FileInputStream;
import java.util.ArrayList; 
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import controller.InventoryController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import model.Category;
import model.Item;
/**
 * 
 * @author Reija Parvio, Jensina Hakkarainen, Emil Rantanen
 * Käyttöliittymä varastonhallinta-ohjelmalle
 *
 */

public class BasicGUI extends Application {

	private InventoryController inventorycontroller;
	private LoginGUI login = new LoginGUI();
	
	private TableView<Item> itemTable = new TableView<>();
	private Image itemIcon = new Image("item_icon_small.png");
	private ImageView itemIconView = new ImageView(itemIcon);
	private Image userIcon = new Image("User_icon_BLACK-01.png");
	private ImageView userIconView = new ImageView(userIcon);
	private Stage window;
	private Locale curLocale;
	private VBox loanList = new VBox();
	private VBox selectedItems = new VBox();
	private Label selectedItem = new Label();
	private Button loan = new Button();
	private Label loanHeader = new Label();
	
	//Kielen perusteella vaihtuvat muuttujat 
	private String loanHeaderText;
	private String buttonLoanText;
	private String column1Name;
	private String column2Name;
	private String welcomeText;
	private String instructionsText;
	private String buttonLogInText;
	private String finLangText, sveLangText, plLangText;
	public String userNameText, passwordText, cancelText;
	
	private TableColumn<Item, String> column1;
	private TableColumn<Item, String> column2;
	private Label welcome;
	private Label instructions;
	private Button buttonLogIn;
	private Button finLang;
	private Button sveLang;
	private Button plLang;
	
	private Image finlandIcon = new Image("Finland-icon.png");
	private BackgroundImage finBI;
	private Background finBg;
	
	private Image swedenIcon = new Image("Sweden-icon.png");
	private BackgroundImage sweBI;
	private Background sweBg;
	
	private Image polandIcon = new Image("Poland-icon.png");
	private BackgroundImage plBI;
	private Background plBg;
	
	//Kielen konfiguraatio-tiedosto on alustettu default-tiedostolla, jossa on kielenä suomi
	private String configFile = "src/main/java/resources/InventoryApp.properties";
	
	private boolean attempt = false;
	
	private Properties properties = new Properties();
	private ResourceBundle bundle;

	/**
	 * Start-metodissa luodaan stage, jolle lisätään scene, johon on lisätty borderPane
	 * BorderPane:e täytetään kutsumalla eri metodeja
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		
		finLang = new Button(finLangText); 
		sveLang = new Button(sveLangText);
		plLang = new Button(plLangText);
		
		primaryStage.setTitle("InventoryMate");
		

		BorderPane root = new BorderPane();
		root.setPrefSize(1000, 700);
		
		root.setTop(addLogIn());
		root.setLeft(addAnchorPane(addNavigation()));
		root.setCenter(addTable());
		root.setRight(addLoanEvent());
		
		//Maiden liput laitetaan kielipainikkeiden taustakuviksi
		BackgroundImage finBI = new BackgroundImage(finlandIcon, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(finLang.getWidth(), finLang.getHeight(), true, true, true, false));
		Background finBg = new Background(finBI);
		
		BackgroundImage sweBI = new BackgroundImage(swedenIcon, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(finLang.getWidth(), finLang.getHeight(), true, true, true, false));
		Background sweBg = new Background(sweBI);
		
		BackgroundImage plBI = new BackgroundImage(polandIcon, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(finLang.getWidth(), finLang.getHeight(), true, true, true, false));
		Background plBg = new Background(plBI);
		
		finLang.setBackground(finBg);
		sveLang.setBackground(sweBg);
		plLang.setBackground(plBg);
		
		/**
		 * Kieltenvalinta-painikkeet päivittävät konfiguraatiotiedoston
		 * ja sen ohella kaikki kieliasetukset vaativat elementit
		 */
		
		finLang.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setConfigFile("src/main/java/resources/InventoryAppFI.properties"); //suomenkieliset kieliasetukset löytyvät täältä
				System.out.println(configFile);
				updateLanguage(configFile);
				buttonLogIn.setText(buttonLogInText);
				welcome.setText(welcomeText);
				column1.setText(column1Name);
				column2.setText(column2Name);
				instructions.setText(instructionsText);
				loan.setText(buttonLoanText);
				loanHeader.setText(loanHeaderText);
			}
		});
		
		sveLang.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setConfigFile("src/main/java/resources/InventoryAppSE.properties"); //ruotsinkieliset kieliasetukset löytyvät täältä
				System.out.println(configFile);
				updateLanguage(configFile);
				buttonLogIn.setText(buttonLogInText);
				welcome.setText(welcomeText);
				column1.setText(column1Name);
				column2.setText(column2Name);
				instructions.setText(instructionsText);
				loan.setText(buttonLoanText);
				loanHeader.setText(loanHeaderText);
			}
		});
		
		plLang.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setConfigFile("src/main/java/resources/InventoryAppPL.properties"); //puolankieliset kieliasetukset löytyvät täältä
				System.out.println(configFile);
				updateLanguage(configFile);
				buttonLogIn.setText(buttonLogInText);
				welcome.setText(welcomeText);
				column1.setText(column1Name);
				column2.setText(column2Name);
				instructions.setText(instructionsText);
				loan.setText(buttonLoanText);
				loanHeader.setText(loanHeaderText);
			}
		});
		
		/**
		 * Sisäänkirjautumispainike avaa kirjautumisikkunan metodilla showLoginGUI()
		 */
		
		buttonLogIn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Nappi toimii");
				List<String> loginLanguage = new ArrayList<String>();
				loginLanguage.add(buttonLogInText);
				loginLanguage.add(welcomeText);
				loginLanguage.add(userNameText);
				loginLanguage.add(passwordText);
				loginLanguage.add(cancelText);
				boolean attempt = login.showLoginGUI(loginLanguage);
			}
		});
		
		
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	
	/**
	 * VBox elementti, joka pitää sisällään lainaustapahtuman elementit
	 * @return VBox-elementti
	 */
	
	public VBox addLoanEvent() {
		
		loanList.setPrefSize(300, 600);
		loanList.setStyle("-fx-background-color: #F3F6EC;");
		
		loanList.getChildren().add(selectedItems);
		
		return loanList;
	}
	
	/**
	 * VBox-elementti tervehdystekstille ja ohjeistukselle
	 * VBox-elementin sisälle luodaan toinen VBox taulukolle, johon tulostuu tietokannan välineet sekä niiden kategoriat
	 * @return VBox-elementti
	 */
	public VBox addTable() {
		VBox itemScene = new VBox();
		
		itemScene.setPadding(new Insets(15, 12, 15, 12));
		itemTable.setItems(inventorycontroller.getItemList());
		itemTable.setPrefSize(400, 400);
		
		column1 = new TableColumn<Item, String>(column1Name);
	    column1.setCellValueFactory(new PropertyValueFactory<Item, String>("name")); //Item-luokasta haetaan kaikkien name-muuttujien tiedot
	    column2 = new TableColumn<Item, String>(column2Name);
	    column2.setCellValueFactory(new PropertyValueFactory<Item, String>("category")); //Item-luokasta haetaan kaikkien category-muuttujien tiedot
	    column1.setPrefWidth(200);
	    column2.setPrefWidth(200);

	    itemTable.getColumns().setAll(column1, column2);
	    itemTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	    itemTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Item>() {
	    	
	    	/**
	    	 * Taulukon sarakkeen painaminen käynnistää changed-metodin
	    	 * Metodissa muokataan ja asetetaan elementit loanList-VBoxiin
	    	 */

			@Override
			public void changed(ObservableValue<? extends Item> startValue, Item oldValue, Item newValue) {
				if(itemTable.getSelectionModel().getSelectedItem() != null) {
					TableViewSelectionModel selectionmodel = itemTable.getSelectionModel();
					ObservableList selectedcells = selectionmodel.getSelectedCells();
					TablePosition tableposition = (TablePosition) selectedcells.get(0);
					Object value = tableposition.getTableColumn().getCellData(newValue);
					selectedItem.setText((String) value);
					
					selectedItems.getChildren().addAll(loanHeader, selectedItem, loan);
					
					loanHeader.setText(loanHeaderText);
					loanHeader.setFont(Font.font("Verdana", FontWeight.BOLD, 15));
					
					selectedItems.setPadding(new Insets(10, 10, 10, 10));
					selectedItems.prefHeight(400);
					loanList.setMargin(selectedItems, new Insets(150, 30, 10, 10));
					
					selectedItems.setMargin(loan, new Insets(300, 10, 0, 10));
					
					loan.setText(buttonLoanText);
					loan.setPrefSize(200, 40);
					loan.setAlignment(Pos.CENTER);
					loan.setFont(Font.font ("Verdana", 20));
					loan.setStyle("-fx-background-color: #94AE94; ");
					
					selectedItems.setStyle("-fx-border-style: solid inside;" + 
											"-fx-border-color: #94AE94;");
				}
				
			}
	    	
	    });
	    
	    welcome = new Label(welcomeText);
	    instructions = new Label(instructionsText);
	    
	    welcome.setFont(Font.font ("Verdana", 20));
	    welcome.setPadding(new Insets(10, 0, 10, 0));
		
		VBox itemListBox = new VBox();
		itemListBox.getChildren().addAll(itemTable);
		
		itemListBox.setPadding(new Insets(70, 12, 15, 12));
		itemListBox.setSpacing(10);

		itemScene.getChildren().addAll(welcome, instructions, itemListBox);
		
		itemScene.setStyle("-fx-background-color: #F3F6EC;");

		
		return itemScene;
	}
	
	/**
	 * VBox-elementti pystynavigointipalkille, jossa on valittavissa välineiden tarkastelu tai henkilökohtaiset tiedot
	 * @return VBox-elementti
	 */
	public VBox addNavigation() {
		VBox leftNavi = new VBox();
		
		leftNavi.setPrefSize(200, 700);
		leftNavi.setStyle("-fx-background-color: #94AE94;");
		
		Button buttonItems = new Button();
		Button buttonPersonal  = new Button();
		
		buttonItems.setGraphic(itemIconView);
		buttonPersonal.setGraphic(userIconView);
		
		buttonItems.setPrefSize(200, 200);
		buttonPersonal.setPrefSize(200, 200);
		
		leftNavi.getChildren().addAll(buttonItems, buttonPersonal);
		
		return leftNavi;
	}
	
	/**
	 * ankkuroitu ruutu vasemman sivun pystynavigointipalkkia varten, jotta se pysyisi halutussa koossa ikkunan koosta huolimatta
	 * @param leftNavi, vasemman sivun VBox-elementti
	 * @return AnchorPane-elementti
	 */
	public AnchorPane addAnchorPane(VBox leftNavi) {
		AnchorPane anchorPaneLeft = new AnchorPane();
		anchorPaneLeft.getChildren().addAll(leftNavi);
		anchorPaneLeft.setLeftAnchor(leftNavi, 2.0);
		anchorPaneLeft.setTopAnchor(leftNavi, 0.0);
		
		return anchorPaneLeft;
		
	}
	
	/**
	 * HBox-elementti ylänavigointipalkille
	 * Navigointipalkkiin luodaan napit sisäänkirjautumiselle sekä kielivalinnoille
	 * @return HBox-elementti
	 */
	public HBox addLogIn() {
		HBox logInNavi = new HBox();
		logInNavi.setPadding(new Insets(15, 12, 15, 12));
		logInNavi.setSpacing(10);
		logInNavi.setStyle("-fx-background-color: DAE6DA;");
		
		buttonLogIn = new Button(buttonLogInText);
		buttonLogIn.setPrefSize(200, 40);
		buttonLogIn.setFont(Font.font ("Verdana", 20));
		buttonLogIn.setStyle("-fx-background-color: #94AE94; ");
		
		finLang.setPrefSize(30, 30);
		finLang.setFont(Font.font ("Verdana", 10));
		
		
		sveLang.setPrefSize(30, 30);
		sveLang.setFont(Font.font ("Verdana", 10));
		
		plLang.setPrefSize(30, 30);
		plLang.setFont(Font.font ("Verdana", 10));
		
		
		logInNavi.getChildren().addAll(finLang,sveLang,plLang, buttonLogIn);
		logInNavi.setAlignment(Pos.CENTER_RIGHT);
		
		return logInNavi;
		
		
	}
	
	//Setterit ja getterit kaikille elementeille, joita kielivalinnat koskee
	
	public void setButtonLoanText(String buttonLoanText) {
		this.buttonLoanText = buttonLoanText;
	}
	
	public String getButtonLoanText() {
		return buttonLoanText;
	}
	
	public void setLoanHeaderText(String loanHeaderText) {
		this.loanHeaderText = loanHeaderText;
	}
	
	public String getLoanHeaderText() {
		return loanHeaderText;
	}
	
	private void setColumn1Name(String column1Name) {
		this.column1Name = column1Name;
	}
	
	public String getColumn1Name() {
		return this.column1Name;
	}
	
	private void setColumn2Name(String column2Name) {
		this.column2Name = column2Name;
	}
	
	public String getColumn2Name() {
		return this.column2Name;
	}
	
	private void setWelcomeText(String welcome) {
		this.welcomeText = welcome;
	}
	
	private void setInstructionsText(String it) {
		this.instructionsText = it;
	}
	
	
	private void setLogIn(String logInText) {
		this.buttonLogInText = logInText;
	}

	public String getConfigFile() {
		return this.configFile;
	}
	
	private void setConfigFile(String file) {
		this.configFile = file;
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * Ladataan kielen konfiguraatio-tiedosto ja luodaan sen maa- ja kielikoodin perusteella uusi Locale-olio,
	 * joka laitetaan default-valinnaksi. 
	 * 
	 * Kaikille kieltä sisältäville elementeille haetaan oikeat vastineet siitä bundlesta, 
	 * minkä kielen konfiguraatio-tiedosto on haettu configFile-muuttujaan
	 * 
	 * @param configFile, default-kielen konfiguraatiotiedoston osoite
	 */
	
	public void updateLanguage(String configFile) {
		
		try {
			properties.load(new FileInputStream(getConfigFile()));
			String language = properties.getProperty("language");
			String country = properties.getProperty("country");
			
			curLocale = new Locale(language, country);
			Locale.setDefault(curLocale);
		}catch (Exception e) {
			System.out.println(e);
		}
		
		try {
			bundle = ResourceBundle.getBundle("resources/TextResources", curLocale);
			column1Name = bundle.getString("column1Name");
			setColumn1Name(column1Name);
			column2Name = bundle.getString("column2Name");
			setColumn2Name(column2Name);
			welcomeText = bundle.getString("welcomeText");
			setWelcomeText(welcomeText);
			instructionsText = bundle.getString("instructionsText");
			setInstructionsText(instructionsText);
			buttonLogInText = bundle.getString("buttonLogInText");
			setLogIn(buttonLogInText);
			userNameText = bundle.getString("userNameText");
			passwordText = bundle.getString("passwordText");
			cancelText = bundle.getString("cancelText");
			buttonLoanText = bundle.getString("buttonLoanText");
			setButtonLoanText(buttonLoanText);
			loanHeaderText = bundle.getString("loanHeaderText");
			setLoanHeaderText(loanHeaderText);
		}catch (Exception e) {
			System.out.println(e);
			System.exit(0);
		}
	}
	
	/**
	 * Luodaan instanssi kontrollerista
	 * Kutsutaan kielenvalinta-metodia
	 */
	
	public void init() {
		
		updateLanguage(this.configFile);
		
		inventorycontroller = new InventoryController(this);
		
	}

}

