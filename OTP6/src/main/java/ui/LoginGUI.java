package ui;

import controller.LoginController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * @author Emil Rantanen
 *
 *         Kirjautumisruudun käyttöliittymä.
 *
 */

public class LoginGUI {

	private LoginController controller = new LoginController();
	/**
	 * showLoginGUI avaa kirjautumisnäkymän josta lähdetään kutsumaan itse kirjautumista totetuttavia metodeja
	 * @param loginLanguage pääkäyttöliittymästä
	 * @return kirjautumisen onnistuminen
	 */

	public boolean showLoginGUI(List<String> loginLanguage) {
		
		Stage dialogStage = new Stage();
		dialogStage.setTitle(loginLanguage.get(0));
		dialogStage.initModality(Modality.WINDOW_MODAL);
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		Text scenetitle = new Text(loginLanguage.get(1) + "!");
		grid.add(scenetitle, 0, 0, 2, 1);

		Label userName = new Label(loginLanguage.get(2) + ":");
		grid.add(userName, 0, 1);

		TextField userTextField = new TextField();
		grid.add(userTextField, 1, 1);

		Label pw = new Label(loginLanguage.get(3) + ":");
		grid.add(pw, 0, 2);

		PasswordField pwBox = new PasswordField();
		grid.add(pwBox, 1, 2);

		Button btn = new Button(loginLanguage.get(0));
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_LEFT);

		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 4);

		Button btn2 = new Button(loginLanguage.get(4));
		HBox hbBtn2 = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn2);
		grid.add(hbBtn2, 2, 4);

		final Text actiontarget = new Text();
		grid.add(actiontarget, 1, 6);

		btn.setOnAction(new EventHandler<ActionEvent>() {

			// OK-nappia painettaessa otetaan tunnus- ja salasanakenttien arvot ja lähetetään ne LoginControllerin handleOK-metodiin
			@Override
			public void handle(ActionEvent e) {
				System.out.println("Login 1");
				String idField = userTextField.getText();
				String pinField = pwBox.getText();

				controller.setId(idField);
				controller.setPin(pinField);

				controller.handleOk();
				if (controller.isOkClicked()) dialogStage.close();
			}
		});

		// Jos cancel suljetaan ruutu
		btn2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				System.out.println("Login 2");

				dialogStage.close();
			}
		});

		Scene scene = new Scene(grid, 325, 250);
		dialogStage.setScene(scene);

		dialogStage.showAndWait();

		return controller.isOkClicked();
	}
}
